//
//  rezyser.hpp
//  Projekt2
//
//  Created by Maciej  Święch on 04.04.2020.
//  Copyright © 2020 Maciej  Święch. All rights reserved.
//

#ifndef rezyser_hpp
#define rezyser_hpp

#include <stdio.h>

#include <iostream>
#include <vector>

using namespace std;
class Rezyser{
private:
    string imie_rezysera;
    string nazwisko_rezysera;
    int wiek_rezysera;
    vector <string> dziela;
public:
    Rezyser(string imie_rezysera, string nazwisko_rezysera);
    Rezyser();
    
    string get_name();
    string get_surname ();
    int get_age ();
    vector<string> get_dziela();
    void set_age (int age);
    void set_name (string name);
    void set_surname (string surname);
    void set_dziela (string dzielo);
    
    //przeciążenie operatora "<<"
    friend std::ostream& operator<< (std::ostream &out, const Rezyser &rezyser_1);
    
    
};

#endif /* rezyser_hpp */
