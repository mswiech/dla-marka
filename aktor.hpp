//
//  aktor.hpp
//  Projekt2
//
//  Created by Maciej  Święch on 05.04.2020.
//  Copyright © 2020 Maciej  Święch. All rights reserved.
//

#ifndef aktor_hpp
#define aktor_hpp
#include <string>
#include <vector>

class Aktor {
    
private:
    string imie_aktora;
    string nazwisko_aktora;
    string pesel_aktora;
    int wiek_aktora;
    vector<string>filmy_aktora;
    
public:
    //konstruktory
    Aktor (string aktor_imie, string aktor_nazwisko, string aktor_pesel);
    Aktor ();
    //przeciążenie konstruktora kopiującego
    
    Aktor (const Aktor &copyaktor);
    
    
    //settery
    void set_wiek_aktora(int actors_age);
    void set_imie_aktora(string actors_name);
    void set_nazwisko_aktora(string actors_surname);
    void set_filmy_aktora(vector<string> actors_films);
    void set_pesel_aktora(string actors_pesel);
    //gettery
    string get_imie_aktora() const;
    string get_nazwisko_aktora()const;
    string get_pesel_aktora()const;
    int get_wiek_aktora()const;
    vector<string> get_filmy_aktora()const;
    
    //funkcja do porownywania obiektow klasy Aktor
    //przeciążanie opeartora "=="
    bool operator == (Aktor &aktor1){
        string pesel_aktor_1 = aktor1.get_pesel_aktora();
        //string pesel_aktor_2 = aktor2.get_pesel_aktora();
        int licznik_bledow = 0;
        if (this -> pesel_aktora.length() != pesel_aktor_1.length()){
            licznik_bledow = 1;
        }
        else {
            for (int i = 0; i < pesel_aktor_1.length(); i++){
                if (this -> pesel_aktora[i] != pesel_aktor_1[i]){
                    licznik_bledow ++;
                }
            }
        }
        if (licznik_bledow > 0)
            return false;
        else
            return true;
    }
    
    //przeciążenie operatora "!="
    bool operator != (Aktor &aktor2){
        string pesel_aktor_2 = aktor2.get_pesel_aktora();
        //string pesel_aktor_2 = aktor2.get_pesel_aktora();
        int licznik_bledow2 = 0;
        if (this -> pesel_aktora.length() != pesel_aktor_2.length()){
            licznik_bledow2 = 1;
        }
        else {
            for (int i = 0; i < pesel_aktor_2.length(); i++){
                if (this -> pesel_aktora[i] != pesel_aktor_2[i]){
                    licznik_bledow2 ++;
                }
            }
        }
        if (licznik_bledow2 > 0)
            return true;
        else
            return false;
    }
    //przeciążenie operatora "<<"
    friend std::ostream& operator<< (std::ostream &out, const Aktor &aktor_1);
    //przeciążenie operatora "="
    Aktor operator = (const Aktor &aktor3){
        Aktor nowyaktor;
        string noweimie = aktor3.get_imie_aktora();
        string nowenazwisko = aktor3.get_nazwisko_aktora();
        string nowypesel = aktor3.get_pesel_aktora();
        int nowywiek = aktor3.get_wiek_aktora();
        vector<string>nowefilmy  = aktor3.get_filmy_aktora();
        nowyaktor.set_imie_aktora(noweimie);
        nowyaktor.set_nazwisko_aktora(nowenazwisko);
        nowyaktor.set_wiek_aktora(nowywiek);
        nowyaktor.set_pesel_aktora(nowypesel);
        nowyaktor.set_filmy_aktora(nowefilmy);
        return nowyaktor;

    }
    
};

#endif /* aktor_hpp */
