//
//  rezyser.cpp
//  Projekt2
//
//  Created by Maciej  Święch on 04.04.2020.
//  Copyright © 2020 Maciej  Święch. All rights reserved.
//

#include "rezyser.hpp"
#include "sztuka.hpp"
#include "aktor.hpp"
#include "Test.hpp"
#include <string>
#include <iostream>
#include <vector>

using namespace std;


//
//implementacja konstruktorów klasy Aktor
//

Aktor::Aktor(string aktorimie, string aktornazwisko, string aktorpesel){
    this -> imie_aktora = aktorimie;
    this -> nazwisko_aktora = aktornazwisko;
    this -> pesel_aktora = aktorpesel;
    //cout << imie_aktora << nazwisko_aktora;
}
Aktor::Aktor(){}

//przeciążenie konstruktora kopiującego klasy Aktor
Aktor::Aktor (const Aktor &copyaktor){
    imie_aktora = copyaktor.get_imie_aktora();
    nazwisko_aktora = copyaktor.get_nazwisko_aktora();
    pesel_aktora = copyaktor.get_pesel_aktora();
    wiek_aktora = copyaktor.get_wiek_aktora();
    filmy_aktora = copyaktor.get_filmy_aktora();
}



//
//Gettery i Settery dla klasy "Aktor"
//
//Gettery:
string Aktor::get_imie_aktora()const{
    return this -> imie_aktora;
}
string Aktor::get_nazwisko_aktora()const{
    return this -> nazwisko_aktora;
}
string Aktor::get_pesel_aktora()const{
    return this -> pesel_aktora;
}

int Aktor::get_wiek_aktora()const{
    return this -> wiek_aktora;
}
vector<string> Aktor::get_filmy_aktora()const{
    return this -> filmy_aktora;
}


//Settery:
void Aktor::set_imie_aktora(string actors_name){
    this -> imie_aktora = actors_name;
}
void Aktor::set_wiek_aktora(int actors_age){
    this -> wiek_aktora = actors_age;
}
void Aktor::set_nazwisko_aktora(string actors_surname){
    this -> nazwisko_aktora = actors_surname;
}
void Aktor::set_filmy_aktora(vector<string> actors_films){
    this -> filmy_aktora = actors_films;
}
void Aktor::set_pesel_aktora(string actors_pesel){
    this -> pesel_aktora = actors_pesel;
}
// przeciążanie operatora <<
ostream& operator<< (ostream &out, const Aktor &aktor_1)
{
    out << "Dane Aktora: " << aktor_1.imie_aktora << " "<< aktor_1.nazwisko_aktora << "PESEL: "<< aktor_1.pesel_aktora;
    return out;
}

//
//Gettery i Settery dla klasy "Rezyser"
//
//Gettery:
string Rezyser::get_name(){
    return this -> imie_rezysera;
}
string Rezyser::get_surname (){
    return this -> nazwisko_rezysera;
}
int Rezyser::get_age (){
    return this -> wiek_rezysera;
}
vector<string> Rezyser::get_dziela(){
    return this -> dziela;
}
//Settery:
void Rezyser::set_age (int age){
    this -> wiek_rezysera = age;
}
void Rezyser::set_name(string name){
    this -> imie_rezysera = name;
}
void Rezyser:: set_surname(string surname){
    this -> nazwisko_rezysera = surname;
}
void Rezyser:: set_dziela(string dzielo){
    this -> dziela.push_back(dzielo);
}
// przeciążanie operatora <<
ostream& operator<< (ostream &out, const Rezyser &rezyser_1)
{
    out << "Dane Rezysera: " << rezyser_1.imie_rezysera << " "<< rezyser_1.nazwisko_rezysera ;
    return out;
}



//
//Gettery i Settery dla klasy "Sztuka"
//

//Gettery:
string Sztuka::get_tytul(){
    return this -> tytul_sztuki;
}
string Sztuka::get_nazwa_sztuki(){
    return this -> nazwa_sztuki;
}
vector <Aktor> Sztuka::get_aktorzy(){
    return this -> aktorzy;
}
Rezyser Sztuka::get_rezyser(){
    return this -> rezyser_sztuki;
}
//Settery:
void Sztuka::set_nazwa_sztuk(string name_sztuki){
    this -> nazwa_sztuki = name_sztuki;
}
void Sztuka::set_tytul_sztuki(string title_sztuki){
    this -> tytul_sztuki = title_sztuki;
}
void Sztuka::set_rezyser(string name_of_new_rezyser, string surname_of_new_rezyser){
    this -> rezyser_sztuki.set_name(name_of_new_rezyser);
    this -> rezyser_sztuki.set_surname(surname_of_new_rezyser);
}
vector<Aktor> Sztuka::set_aktorzy(Aktor new_actors){
    this -> aktorzy.push_back(new_actors);
    return this -> aktorzy;
}
vector<Aktor> Sztuka::usun_aktor(Aktor aktor_do_usuniecia){
    vector<Aktor> lista_aktorow = this -> aktorzy;
    for (int i = 0; i < lista_aktorow.size(); i++){
        if (lista_aktorow[i] == aktor_do_usuniecia){
            lista_aktorow.erase(lista_aktorow.begin() + i);
        }
    }
    this -> aktorzy = lista_aktorow;
    return aktorzy;
}
// przeciążanie operatora <<
ostream& operator<< (ostream &out, const Sztuka &sztuka_1)
{
    out << "Tytul sztuki: " << sztuka_1.nazwa_sztuki;
    return out;
}




//
//Konstruktory klasy Rezyser
//
Rezyser::Rezyser(string imie, string nazwisko){

    this -> imie_rezysera = imie;
    this -> nazwisko_rezysera = nazwisko;
}
Rezyser::Rezyser(){}

//
//Konstruktory klasy Sztuka
//
Sztuka::Sztuka(){}

Sztuka::Sztuka(string sztuki_nazwa, string sztuki_tytul, Rezyser sztuki_rezyser){

    this -> nazwa_sztuki = sztuki_nazwa;
    this -> tytul_sztuki = sztuki_tytul;
    Rezyser rezyser_sztuki = sztuki_rezyser;
    string rezysera_imie = rezyser_sztuki.get_name();
    string rezysera_nazwisko = rezyser_sztuki.get_surname();
    int rezysera_wiek = rezyser_sztuki.get_age();

    cout <<"Nazwa Sztuki: "<< nazwa_sztuki << endl;
    cout <<"Tytuł sztuki: "<< tytul_sztuki << endl;
    cout <<"Dane reżysera: "<< endl;
    cout <<"Imie: "<<rezysera_imie << endl;
    cout <<"Nazwisko: "<<rezysera_nazwisko << endl;
}
//
//impelentacja konstuktorów klasy Test
//
Test::Test(){
    cout << "Witamy w programie" << endl;
    cout << "Podaj dane rezysera :" << endl;
    string imierezysera, nazwiskorezysera, imieaktora, nazwiskoaktora, peselaktora, nazwasztuki, tytulsztuki;
    cout << "Imie rezysera: ";
    cin >> imierezysera;
    cout << "Nazwisko rezysera: ";
    cin >> nazwiskorezysera;
    cout << "Imie aktora: ";
    cin >> imieaktora;
    cout << "Nazwisko aktora: ";
    cin >> nazwiskoaktora;
    cout << "Pesel aktora: ";
    cin >> peselaktora;
    cout << "Nazwa sztuki: ";
    cin >> nazwasztuki;
    cout << "Tytul sztuki: ";
    cin >> tytulsztuki;
    
    
    this -> test_aktor = Aktor(imieaktora, nazwiskoaktora, peselaktora);
    test_aktor.set_imie_aktora(imieaktora);
    test_aktor.set_nazwisko_aktora(nazwiskoaktora);
    test_aktor.set_pesel_aktora(peselaktora);
    this -> test_rezyser = this -> set_test_rezyser(imierezysera, nazwiskorezysera);
    test_rezyser.set_name(imierezysera);
    test_rezyser.set_surname(nazwiskorezysera);
    this -> test_sztuka = this -> set_test_sztuka(nazwasztuki, tytulsztuki, this -> test_rezyser);
    test_sztuka.set_nazwa_sztuk(nazwasztuki);
    test_sztuka.set_tytul_sztuki(tytulsztuki);
    test_sztuka.set_rezyser(test_rezyser.get_name(), test_rezyser.get_surname());
    
}
//
//Gettery i Settery dla klasy "Test"
//

//Gettery:

string Test::get_test_aktor_name(){
    return this -> test_imie_aktora;
}
string Test::get_test_aktor_surname (){
    return this -> test_nazwisko_aktora;
}
string Test::get_test_aktor_pesel(){
    return this -> test_pesel_aktora;
}
string Test::get_test_rezyser_name(){
    return this -> test_imie_rezysera;
}
string Test::get_test_rezyser_surname(){
    return this -> test_nazwisko_rezysera;
}

Aktor Test::set_test_aktor(string test_imie_akt, string test_nazwisko_akt, string test_pesel_akt){
    this -> test_aktor = Aktor (test_imie_akt, test_nazwisko_akt, test_pesel_akt);
    return this ->test_aktor;
}
Rezyser Test::set_test_rezyser(string test_imie_rez, string test_nazwisko_rez){
    this -> test_rezyser = Rezyser ( test_imie_rez, test_nazwisko_rez );
    return this ->test_rezyser;
}
Sztuka Test::set_test_sztuka(string test_nazwa_szt, string test_tytul_szt, Rezyser test_rezyser_szt){
    this -> test_sztuka = Sztuka (test_nazwa_szt, test_tytul_szt, test_rezyser_szt);
    return this ->test_sztuka;
}


int main(){

    Test test1;
    test1.rozpocznij_test();
    
return 0;
}
