//
//  Test.hpp
//  Projekt2
//
//  Created by Maciej  Święch on 06.04.2020.
//  Copyright © 2020 Maciej  Święch. All rights reserved.
//

#ifndef Test_hpp
#define Test_hpp
#include <string>
#include <vector>
#include "aktor.hpp"
#include "rezyser.hpp"
#include "sztuka.hpp"
class Test{
private:
    string test_nazwa_sztuki;
    string test_tytul_sztki;
    string test_imie_rezysera;
    string test_nazwisko_rezysera;
    int test_wiek_rezysera;
    string test_imie_aktora;
    string test_nazwisko_aktora;
    string test_pesel_aktora;
    Aktor test_aktor;
    Rezyser test_rezyser;
    Sztuka test_sztuka;

public:
    Test();
    Aktor set_test_aktor (string test_imie_akt, string test_nazwisko_akt, string test_pesel_akt);
    Rezyser set_test_rezyser (string test_imie_rez, string test_nazwisko_rez);
    Sztuka set_test_sztuka (string test_nazwa_szt, string test_tytul_szt, Rezyser test_rezyser_szt);
    string get_test_aktor_name();
    string get_test_aktor_surname();
    string get_test_aktor_pesel();
    string get_test_rezyser_name();
    string get_test_rezyser_surname();
    
    void rozpocznij_test(){
        cout << "Obiekty stworzone przez konstruktor klasy Test" << endl;
        cout << test_aktor;
        cout << test_rezyser;
        cout << test_sztuka;
        cout << "Jest to funkcja testowa. Sprawdza poprawność wykonywania zadań z polecenia"<<endl;
    }
        
    
};

#endif /* Test_hpp */
