//
//  sztuka.hpp
//  Projekt2
//
//  Created by Maciej  Święch on 05.04.2020.
//  Copyright © 2020 Maciej  Święch. All rights reserved.
//

#ifndef sztuka_hpp
#define sztuka_hpp

#include "rezyser.hpp"
#include "aktor.hpp"
#include <vector>
#include <string>
using namespace std;

class Sztuka {
private:
    
    string nazwa_sztuki;
    string tytul_sztuki;
    vector <Aktor> aktorzy;
    Rezyser rezyser_sztuki;
public:
    Sztuka(string nazwa, string title, Rezyser rezyser_sztuki);
    Sztuka();
    string get_nazwa_sztuki();
    string get_tytul();
    vector <Aktor> get_aktorzy();
    Rezyser get_rezyser();
    void set_nazwa_sztuk(string name_sztuki);
    void set_tytul_sztuki(string title_sztuki);
    vector<Aktor> set_aktorzy (Aktor new_actors);
    void set_rezyser (string name_of_new_rezyser, string surname_of_new_rezyser);
    vector<Aktor> usun_aktor (Aktor aktor_do_usuniecia);
    
    //przeciążenie operatora "<<"
    friend std::ostream& operator<< (std::ostream &out, const Sztuka &sztuka_1);
    
};


#endif /* sztuka_hpp */
